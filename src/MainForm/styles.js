import styled from "styled-components";
import img from "../assets/images/wunderMobility.png";

export const MainContainer = styled.div`
  display: flex;
  width: 100vw;
  height: 100vh;

  align-items: center;
  justify-content: center;
`;

export const BackgroundImage = styled.div`
  width: 100%;
  height: 100%;

  position: absolute;

  background-image: url(${img});
  background-position: center center;
  background-repeat: no-repeat;
  background-attachment: fixed;
  background-size: cover;

  -webkit-filter: blur(5px);
  -moz-filter: blur(5px);
  filter: blur(5px);

  z-index: 1;
`;

export const FormContainer = styled.div`
  width: 40%;
  z-index: 2;
`;

export const ButtonsContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;

  width: 100%;

  padding: 1em;
`;
