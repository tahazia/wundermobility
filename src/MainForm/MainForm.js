import React, { Component } from "react";
import axios from "axios";
import localStorage from "local-storage";

import { Step1, Step2, Step3, InfoModal, SignUpModal } from "./components";
import LoadingOverlay from "../components/LoadingOverlay";
import Button from "../components/Button";

import {
  MainContainer,
  BackgroundImage,
  FormContainer,
  ButtonsContainer
} from "./styles";

class MainForm extends Component {
  state = {
    stepValue: 0,
    firstName: "",
    lastName: "",
    telephoneNumber: "",
    street: "",
    houseNumber: "",
    zipCode: "",
    city: "",
    accountHolder: "",
    iban: "",
    paymentId: "",
    loading: false
  };

  componentDidMount() {
    const stepValue = localStorage.get("stepValue");
    const firstName = JSON.parse(localStorage.get("firstName"));
    const lastName = JSON.parse(localStorage.get("lastName"));
    const telephoneNumber = JSON.parse(localStorage.get("telephoneNumber"));
    const street = JSON.parse(localStorage.get("street"));
    const houseNumber = JSON.parse(localStorage.get("houseNumber"));
    const zipCode = JSON.parse(localStorage.get("zipCode"));
    const city = JSON.parse(localStorage.get("city"));

    stepValue
      ? this.setState({ stepValue: parseInt(stepValue) })
      : this.setState({ stepValue: 0 });
    firstName ? this.setState({ firstName }) : this.setState({ firstName: "" });
    lastName ? this.setState({ lastName }) : this.setState({ lastName: "" });
    telephoneNumber
      ? this.setState({ telephoneNumber })
      : this.setState({ telephoneNumber: "" });
    street ? this.setState({ street }) : this.setState({ street: "" });
    houseNumber
      ? this.setState({ houseNumber })
      : this.setState({ houseNumber: "" });
    zipCode ? this.setState({ zipCode }) : this.setState({ zipCode: "" });
    city ? this.setState({ city }) : this.setState({ city: "" });
  }

  nextStep = () => {
    const stepValue = this.state.stepValue + 1;

    if (stepValue === 4) {
      this.sendInfo();
    } else {
      this.setState({ stepValue: this.state.stepValue + 1 });
      localStorage.set("stepValue", JSON.stringify(stepValue));
    }
  };

  prevStep = () => {
    this.setState({ stepValue: this.state.stepValue - 1 });
    const stepValue = this.state.stepValue - 1;
    localStorage.set("stepValue", JSON.stringify(stepValue));
  };

  handleChange = event => {
    const { name, value } = event.target;
    this.setState({
      [name]: value
    });

    //Not saving bank information
    if (name !== 'iban' && name !== 'accountHolder'){
      localStorage.set(name, JSON.stringify(value));
    }
  };

  toggleLoading = condition => {
    this.setState({ loading: condition });
  };

  sendInfo = () => {
    const url =
      "https://cors-anywhere.herokuapp.com/https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data/";

    const data = {
      customerId: 1,
      iban: this.state.iban,
      owner: this.state.accountHolder
    };

    this.toggleLoading(true);

    axios
      .post(url, data)
      .then(res => {
        this.toggleLoading(false);
        this.setState({ stepValue: 4 });
        if (res.status === 200) {
          this.setState({ paymentId: res.data.paymentDataId });
        }
      })
      .catch(err => {
        this.toggleLoading(false);
        this.setState({ stepValue: 4 });
        console.log(err);
      });

    localStorage.set("stepValue", JSON.stringify(0));
  };

  render() {
    const {
      stepValue,
      firstName,
      lastName,
      telephoneNumber,
      street,
      zipCode,
      houseNumber,
      iban,
      accountHolder,
      city,
      paymentId,
      loading
    } = this.state;

    return (
      <MainContainer>
        <BackgroundImage />
        <LoadingOverlay isVisible={loading} />
        <FormContainer>
          {stepValue === 0 ? (
            <SignUpModal nextStep={this.nextStep} />
          ) : stepValue === 1 ? (
            <Step1
              firstName={firstName}
              lastName={lastName}
              telephoneNumber={telephoneNumber}
              handleChange={this.handleChange.bind(this)}
            />
          ) : stepValue === 2 ? (
            <Step2
              street={street}
              houseNumber={houseNumber}
              zipCode={zipCode}
              city={city}
              handleChange={this.handleChange.bind(this)}
            />
          ) : stepValue === 3 ? (
            <Step3
              accountHolder={accountHolder}
              iban={iban}
              handleChange={this.handleChange.bind(this)}
            />
          ) : (
            <InfoModal paymentId={paymentId} />
          )}
          {stepValue < 4 && stepValue > 0 ? (
            <ButtonsContainer>
              {stepValue > 1 ? (
                <Button
                  title={"Previous"}
                  onClick={this.prevStep}
                  width={"10em"}
                  height={"2.5em"}
                />
              ) : (
                <div />
              )}
              <Button
                title={"Next"}
                onClick={this.nextStep}
                width={"10em"}
                height={"2.5em"}
              />
            </ButtonsContainer>
          ) : null}
        </FormContainer>
      </MainContainer>
    );
  }
}

export default MainForm;
