import React, { Component } from "react";
import { Form, InputContainer, Title } from "./styles";
import OutlinedInput from "../../components/OutlinedInput";

class Step1 extends Component {
  render() {
    const { firstName, lastName, telephoneNumber, handleChange } = this.props;

    return (
      <Form>
        <Title>Personal information</Title>

        <InputContainer>
          <OutlinedInput
            placeholder={"Add your first name..."}
            type={"text"}
            value={firstName}
            label={"First name"}
            onChange={handleChange}
            fullWidth
            name={"firstName"}
          />
        </InputContainer>

        <InputContainer>
          <OutlinedInput
            placeholder={"Add your last name..."}
            type={"text"}
            value={lastName}
            label={"Last name"}
            onChange={handleChange}
            fullWidth
            name={"lastName"}
          />
        </InputContainer>

        <InputContainer>
          <OutlinedInput
            placeholder={"Add your telephone number..."}
            type={"number"}
            value={telephoneNumber}
            label={"Telephone name"}
            onChange={handleChange}
            fullWidth
            name={"telephoneNumber"}
          />
        </InputContainer>
      </Form>
    );
  }
}

export default Step1;
