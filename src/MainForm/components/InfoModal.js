import React, { Component } from "react";
import { InfoModalContainer, Title, InfoModalDescription } from "./styles";

class InfoModal extends Component {
  render() {
    const { paymentId } = this.props;

    if (paymentId) {
      return (
        <InfoModalContainer>
          <Title>Successful!</Title>
          <InfoModalDescription>
            Congratulations! Your sign up was successful. Your payment id is:{" "}
            {paymentId}
          </InfoModalDescription>
        </InfoModalContainer>
      );
    } else {
      return (
        <InfoModalContainer>
          <Title>Unsuccessful :(</Title>
          <InfoModalDescription>
            We are sorry we could not sign you up at the moment.
          </InfoModalDescription>
        </InfoModalContainer>
      );
    }
  }
}

export default InfoModal;
