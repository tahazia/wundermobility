import React, { Component } from "react";
import { Form, Title, InputContainer } from "./styles";
import OutlinedInput from "../../components/OutlinedInput";

class Step2 extends Component {
  render() {
    const { street, houseNumber, zipCode, city, handleChange } = this.props;

    return (
      <Form>
        <Title>Address</Title>
        <InputContainer>
          <OutlinedInput
            placeholder={"Add your street..."}
            type={"text"}
            value={street}
            label={"Street"}
            onChange={handleChange}
            fullWidth
            name={"street"}
          />
        </InputContainer>

        <InputContainer>
          <OutlinedInput
            placeholder={"Add your house number..."}
            type={"text"}
            value={houseNumber}
            label={"House number"}
            onChange={handleChange}
            fullWidth
            name={"houseNumber"}
          />
        </InputContainer>

        <InputContainer>
          <OutlinedInput
            placeholder={"Add your zip code..."}
            type={"number"}
            value={zipCode}
            label={"Zip code"}
            onChange={handleChange}
            fullWidth
            name={"zipCode"}
          />
        </InputContainer>

        <InputContainer>
          <OutlinedInput
            placeholder={"Add your city..."}
            type={"text"}
            value={city}
            label={"City"}
            onChange={handleChange}
            fullWidth
            name={"city"}
          />
        </InputContainer>
      </Form>
    );
  }
}

export default Step2;
