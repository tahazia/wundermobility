import styled from "styled-components";

export const Form = styled.div`
  display: flex;
  flex-direction: column;
  align-self: center;
  margin: auto;

  width: 100%;

  border: 1px solid #dadada;
  border-radius: 5px;

  background: white;
  box-shadow: 5px 5px 10px #dadada;

  padding: 1em;
  z-index: 2;
`;

export const Title = styled.div`
  display: flex;
  color: #323237;

  font-size: 1.4rem;
  font-weight: 700;
  font-style: normal;
  font-stretch: normal;
  margin-bottom: 1em;

  line-height: normal;
  letter-spacing: normal;
`;

export const InputContainer = styled.div`
  padding: 0.1em 0em;
`;

export const InfoModalContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-self: center;
  margin: auto;

  width: 100%;
  min-height: 150px;

  font-size: 1em;

  border: 1px solid #dadada;
  border-radius: 2px;
  box-shadow: 5px 5px 10px #dadada;

  background: white;

  padding: 1em;
  z-index: 2;
`;

export const InfoModalDescription = styled.div`
    font-size: 1.0em;
    color: #a0a0a0
    line-height: normal;
    letter-spacing: normal;
    overflow-wrap: break-word;
    
`;

export const ButtonContainer = styled.div`
  margin: auto;
  width: 15em;
  padding: 2em 0em;
`;
