import React, { Component } from "react";
import { Form, Title, InputContainer } from "./styles";
import OutlinedInput from "../../components/OutlinedInput";

class Step3 extends Component {
  render() {
    const { accountHolder, iban, handleChange } = this.props;

    return (
      <Form>
        <Title>Bank information</Title>

        <InputContainer>
          <OutlinedInput
            placeholder={"Account holder's name..."}
            type={"text"}
            value={accountHolder}
            label={"Account holder"}
            onChange={handleChange}
            fullWidth
            name={"accountHolder"}
          />
        </InputContainer>

        <InputContainer>
          <OutlinedInput
            placeholder={"Add IBAN..."}
            type={"text"}
            value={iban}
            label={"IBAN"}
            onChange={handleChange}
            fullWidth
            name={"iban"}
          />
        </InputContainer>
      </Form>
    );
  }
}

export default Step3;
