import React, { Component } from "react";
import {
  InfoModalContainer,
  Title,
  InfoModalDescription,
  ButtonContainer
} from "./styles";
import Button from "../../components/Button";

class SignUpModal extends Component {
  render() {
    const { nextStep } = this.props;

    return (
      <InfoModalContainer>
        <Title>Sign up</Title>
        <InfoModalDescription>
          We are excited to get you started with the sign up process. Press the
          button below to get going!
        </InfoModalDescription>
        <ButtonContainer>
          <Button title={"Sign up!"} onClick={nextStep} />
        </ButtonContainer>
      </InfoModalContainer>
    );
  }
}

export default SignUpModal;
