import React from 'react'
import propTypes from 'prop-types'
import styled from 'styled-components'
import { Loader } from './Loader'

const Overlay = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: rgba(255, 255, 255, 0.7);
  z-index: 999999999999;
`

const LoadingOverlay = ({ isVisible, selector }) =>
  isVisible ? (
    <Overlay data-selector={selector}>
      <Loader />
    </Overlay>
  ) : null;

LoadingOverlay.propTypes = {
    isVisible: propTypes.bool.isRequired,
    selector: propTypes.string,
}

LoadingOverlay.defaultProps = {
    selector: null,
}

export default LoadingOverlay
