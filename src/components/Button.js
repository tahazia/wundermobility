import React, { Component } from "react";
import styled from "styled-components";
import propTypes from "prop-types";

const ButtonWrapper = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  width: ${props => (props.width ? props.width : "-webkit-fill-available")};
  height: ${props => (props.height ? props.height : "3em")};
  align-self: ${props => (props.align ? props.align : "center")};
  border-radius: 2em;
  background: linear-gradient(
    to left,
    rgba(0, 102, 255, 1) 0%,
    rgba(51, 153, 255, 1) 100%
  );

  &:hover {
    background: linear-gradient(
      to left,
      rgba(0, 102, 255, 0.8) 0%,
      rgba(51, 153, 255, 0.8) 100%
    );
  }

  box-shadow: 2px 5px 10px -6px #f76b1c;
  cursor: pointer;
  outline-width: 0;

  border: none;
  border-radius: 2em;
  outline-width: 0;

  font-size: 0.875rem;
  font-weight: 500;
  text-align: center;
  color: #fff;
`;

class Button extends Component {
  render() {
    const {
      id,
      type,
      align,
      width,
      title,
      height,
      onClick,
      selector,
      disabled
    } = this.props;
    return (
      <ButtonWrapper
        id={id}
        type={type}
        align={align}
        width={width}
        height={height}
        onClick={onClick}
        disabled={disabled}
        data-selector={selector}
      >
        {title}
      </ButtonWrapper>
    );
  }
}

Button.propTypes = {
  title: propTypes.string.isRequired,
  onClick: propTypes.func.isRequired
};

export default Button;
