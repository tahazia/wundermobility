import React, { Component } from "react";
import propTypes from "prop-types";
import { createMuiTheme, MuiThemeProvider, TextField } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";

const styles = () => ({
  label: {
    paddingLeft: 5,
    paddingRight: 5,
    backgroundColor: "white"
  }
});

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#007dd7",
      dark: "#007deb"
    }
  },
  typography: {
    useNextVariants: true
  }
});

class OutlinedInput extends Component {
  render() {
    const {
      type,
      label,
      name,
      value,
      classes,
      onChange,
      fullWidth,
      autoFocus,
      placeholder
    } = this.props;

    return (
      <MuiThemeProvider theme={theme}>
        <TextField
          name={name}
          type={type}
          label={label}
          value={value || ""}
          autoFocus={autoFocus}
          margin="normal"
          variant="outlined"
          placeholder={placeholder}
          onChange={onChange}
          fullWidth={fullWidth}
          InputLabelProps={{ classes: { root: classes.label }, shrink: true }}
        />
      </MuiThemeProvider>
    );
  }
}

OutlinedInput.propTypes = {
  type: propTypes.string,
  fullWidth: propTypes.bool,
  placeholder: propTypes.string,

  name: propTypes.string.isRequired,
  label: propTypes.string.isRequired,
  value: propTypes.string.isRequired,
  classes: propTypes.object.isRequired,
  onChange: propTypes.func.isRequired
};

OutlinedInput.defaultProps = {
  id: null,
  type: "text",
  fullWidth: false,
  placeholder: ""
};

export default withStyles(styles)(OutlinedInput);
