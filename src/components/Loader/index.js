import React from "react";

import "./index.css";

const Loader = () => {
  return (
    <div className="loader-container">
      <div id="loader">
        <span />
        <span />
        <span />
        <span />
        <span />
      </div>
    </div>
  );
};

export { Loader };
