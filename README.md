Wunder Mobility Challenge

Steps to run:
- npm i (install all dependencies)
- npm start (run application)

--------------------------------------------------

Code optimizations:
- Could have used case to decide which step to execute
- Instead of making step1,2,3, one component could have been abstracted enough
to be used. Affects re-usability of code.

What could have been done better:
- There could have been unit tests to test logic
- Could use Cypress to test views
- Forgot to make git repository in the beginning. Could have made different
branches for different features. Would be easy to version the app.
